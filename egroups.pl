#!/usr/bin/perl

use strict;
use warnings;

#use lib "/usr/lib/perl5/5.8.8/";
use lib "/home/agonzale/bin/";
use Getopt::Long;

#use SOAP::Lite;

use Net::LDAP;
use Net::LDAP::Control::Paged;
use Net::LDAP::Constant qw( LDAP_CONTROL_PAGED );
use AFSLDAP;

use Data::Dumper;

package main;


#------------------------------------------------------------------------------
# Defaults
#------------------------------------------------------------------------------

my %DEF = (# 'ldap_base'=> 'OU=e-groups,OU=Workgroups,dc=cern,dc=ch',
    'ldap_base'=> 'dc=cern,dc=ch',
    'ldap_server' => 'xldap.cern.ch',
    'ldap_base_user' => 'OU=Users,OU=Organic Units,dc=cern,dc=ch',
    #'end_point' => 'https://cra-ws.cern.ch/cra-ws/',
    'end_point' => 'https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService',
    #'xmlns' => 'https://cra-ws.cern.ch/cra-ws/',
    'xmlns' => 'https://foundservices.cern.ch/ws/egroups/v1/schema/EgroupsServicesSchema',
    'Selfsubscription' => 'Closed',
    'UsageCode' => 'SecurityMailing',
    'Description' => 'No description',
    'Privacy' => 'Members',
    'Selfsubscription' => 'Closed',
#   'AdministratorEgroup' => 'VC-Administration', # This is only for VC systems
    'egroups_config' => '/etc/egroups.cfg',
    'egroups_config_user' => "$ENV{HOME}/.egroups/config",
);


#------------------------------------------------------------------------------
# Functions
#------------------------------------------------------------------------------

sub get_employeeID($){
my ($user) = @_;

my $ldap_server;
my $ldap_base;

if($main::SECRET->{ldap_server}){
        $ldap_server=$main::SECRET->{ldap_server};
}else{
        $ldap_server=$DEF{ldap_server};
}

if($main::SECRET->{ldap_base_user}){
        $ldap_base=$main::SECRET->{ldap_base_user};
}else{
        $ldap_base=$DEF{ldap_base_user};
}

warn "Using $ldap_server to get employeeID" if $main::debug;

my $ldap = Net::LDAP->new($ldap_server); 
my $mesg = $ldap->bind;
$mesg->code && die $mesg->error;

warn "Searching filter: (cn=$user) and base: $ldap_base" if $main::debug;

# Take the base to the config file
$mesg = $ldap->search(filter=>"(cn=$user)", base=>$ldap_base);

$ldap->unbind or die "Cannot Disconnect";

my @entries = $mesg->entries;

foreach my $entry (@entries) {
        return $entry->get_value("employeeID");
 }

warn "No user found" if $main::debug;

return 0;
}

sub afsgetUsers($$){

my ($group,$externals) = @_;
my @egroup_members;
my %memory;
my %uMemory;
my $ver=0;

&AFSLDAP::get_egroup_members_flat( $group, \@egroup_members, \%memory , \%uMemory, $ver, $externals);

    if(0==scalar(@egroup_members)){
      # If we get no members, we query LDAP again to see if it is empty or if it doesn't exist.
      if(AFSLDAP::is_group($group)){
		print STDERR "The egroup '$group' is empty.\n";
		exit 5;
      } else { 
		print STDERR "The egroup '$group' doesn't exist.\n";
	#print STDERR "The egroup '$group' doesn't exist\n";
	# It should go to STDERR, but that will increase the number of warnings in the scripts
		exit 4;
       }
    }

return sort(@egroup_members);
}

sub getUsers($$){
my ($group,$externals) = @_;
my $filter;

my $ldap_server;
my $ldap_base;

if($main::SECRET->{ldap_server}){
	$ldap_server=$main::SECRET->{ldap_server};
}else{
        $ldap_server=$DEF{ldap_server};
}

if($main::SECRET->{ldap_base}){
	$ldap_base=$main::SECRET->{ldap_base};
}else{
        $ldap_base=$DEF{ldap_base};
}

$filter = "(|(memberOf:1.2.840.113556.1.4.1941:=cn=$group,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)(memberOf:1.2.840.113556.1.4.1941:=cn=$group,OU=Unix,OU=Workgroups,DC=cern,DC=ch))";

warn "Using $ldap_server" if $main::debug;

my $ldap = Net::LDAP->new($ldap_server) or die "Cannot connect to $ldap_server";
my $mesg = $ldap->bind; # Connect
my $page = Net::LDAP::Control::Paged->new( size => 1000 );
$mesg->code && die $mesg->error;

my $cookie=1;
my @users;

while($cookie){
# Search only the 'member' fields
 $mesg = $ldap->search(
	filter=>$filter,
	base=>$ldap_base,
	control => [ $page ]);

  if ($mesg->code) {
      die "LDAP search error (".$mesg->code."): ".$mesg->error."\n";
  }

print Dumper(\$mesg) if $main::debug;

 my @entries = $mesg->entries;

 if(0==scalar(@entries)){
	if(AFSLDAP::is_group($group)){
		print STDERR "The egroup '$group' is empty.\n";
		exit 5;
	}else{
		print "The egroup '$group' doesn't exist\n";
		exit 4;
	}
 }

 foreach my $entry (@entries) {
	if (defined $entry->get_value("employeeType") and $entry->get_value("employeeType") ne "External"){
	   push (@users, $entry->get_value("sAMAccountName"));
	}
 }

 my ($resp) = $mesg->control( LDAP_CONTROL_PAGED ) or return @users;

 $cookie = $resp->cookie or return @users;
 $page->cookie($cookie);
 }

$ldap->unbind; # Disconnect

return @users;
}

sub init_SOAP(){

my $end_point;

my $action = 'CraEgroupsWebService';

my $username = '';

my $password = '';

if(!$main::SECRET->{p_niceUserid}){ 
  print STDERR "Please write in the configuration file $main::EGROUPS_CONFIG the 'p_niceUserid'\n";
} else {
  $username = $main::SECRET->{p_niceUserid};
}
# else ASK user

if(!$main::SECRET->{p_password}){ 
  print STDERR "Please write in the configuration file $main::EGROUPS_CONFIG the 'p_password'\n";
  $password = get_password();
}else {
  $password = $main::SECRET->{p_password};
}

if($main::SECRET->{end_point}){
	$end_point = $main::SECRET->{end_point};
}else{
	$end_point = $DEF{end_point};
}

return SOAP::Lite->new(
            proxy => [
                $end_point,
                'credentials' => [
                    'foundservices.cern.ch:443',            # host:port
                    'weblogic',                 # realm
                    $username,
                    $password,
                ]
            ]
        );


}

sub deleteEgroup($){
  my ($group) = @_;

 my $yesno=""; 

 print "This will remove the egroup: '$group', are you sure? (This operation cannot be un-done\n";
 
 until($yesno eq "yes" || $yesno eq "no"){
	print "Please write yes or no\n> ";
	$yesno=<>;
	chomp($yesno);
 }

if($yesno ne "yes"){
	print STDERR "Canceling operation, upon user's request\n";
	exit 2;
}

  my $soap = init_SOAP();

  my $xmlns;

if($main::SECRET->{xmlns}){
        $xmlns = $main::SECRET->{xmlns};
}else{
        $xmlns = $DEF{xmlns};
}

  my $soap_response=$soap->call(SOAP::Data->name("DeleteEgroupRequest")
      ->attr({xmlns=>$xmlns})
      => (
          SOAP::Data->name("egroupName" => $group),
          ));

if ($soap_response->result) {
 print "Server response ($group): ".$soap_response->result."\n"; #should be "true" if success
}

if ($soap_response->faultstring) {
 print STDERR "SOAP Error ($group): ".$soap_response->faultstring."\n";
 exit 1;
}

}

sub new_egroup($$$$$$@){
my ($group,$Privacy,$Selfsubscription,$Description,$owner,$AdministratorEgroup,$UsageCode,@members) = @_;

my $error_msg;
my $end_point;
my $xmlns;

my $password;

if(!$main::SECRET->{p_niceUserid}){ 
	$error_msg .= "Please write in the configuration file $main::EGROUPS_CONFIG the 'p_niceUserid'\n";
}
# else ASK user

if(!$main::SECRET->{p_password}){ 
	print "Please write in the configuration file $main::EGROUPS_CONFIG the 'p_password'\n";
  $password = get_password();
}else {
  $password = $main::SECRET->{p_password};
}
# else ask password 

if($main::SECRET->{end_point}){
        $end_point = $main::SECRET->{end_point};
}else{
        $end_point = $DEF{end_point};
}

if($main::SECRET->{xmlns}){
        $xmlns = $main::SECRET->{xmlns};
}else{
        $xmlns = $DEF{xmlns};
}

if(!$end_point){ $error_msg .= "NO end_point found\n"; }

if(!$xmlns){ $error_msg .= "NO xmlns found\n"; }

if(!$owner){  $owner=$ENV{USER}; } 

if(!$owner){ $error_msg .= "NO owner found\n"; }

my $CCID=get_employeeID($owner);

if(!$CCID){ $error_msg .= "Owner ".$owner." not found in ldap\n"; } 

if($error_msg){
	print STDERR "$error_msg\n";
	exit 2;
}

my $soap = init_SOAP();
my $Administrator_def;

if($AdministratorEgroup){
  $Administrator_def=SOAP::Data->name("Administrator" =>
	\SOAP::Data->value(
                       SOAP::Data->name("Name" => $AdministratorEgroup),
                       SOAP::Data->name("Type" => "StaticEgroup"),
	));
}else{
  $Administrator_def=undef;
}

my $group_def = SOAP::Data->name("egroup" =>
	\SOAP::Data->value(
		SOAP::Data->name("Name" => $group),
		#SOAP::Data->name("ID" => 0),  # Not documented, so people cannot use it easily
		SOAP::Data->name("Type" => 'StaticEgroup'), # only this type is supported
		SOAP::Data->name("Usage" => $UsageCode), # No idea what is this??
		SOAP::Data->name("Description" => $Description),
		SOAP::Data->name("Owner" => \SOAP::Data->value(
			SOAP::Data->name("PersonId" => $CCID),
			# SOAP::Data->name("Type" => "Person"),
			)
		),
		SOAP::Data->name("Privacy" => $Privacy),
		SOAP::Data->name("Selfsubscription" => $Selfsubscription),
		$Administrator_def,
		#$members_def->name("Members"),
		)
);

my $soap_response=$soap->call(SOAP::Data->name("SynchronizeEgroupRequest")
      ->attr({xmlns=>$xmlns})
      => (
          #SOAP::Data->name("p_niceUserid" => $main::SECRET->{p_niceUserid}),
          #SOAP::Data->name("p_password" => $password),
          $group_def
          ));

if ($soap_response->result()) {
 print "Server response ($group): ".$soap_response->result()."\n"; #should be "true" if success
}

if ($soap_response->faultstring) {
 print STDERR "SOAP Error ($group): ".$soap_response->faultstring."\n";
 exit 1;
}


addEgroupMembers($group,@members);

}

sub addremoveEgroupMembers($$@){
my ($action,$group,@members) = @_;

my $error_msg;

if(!$main::SECRET->{p_niceUserid}){
        $error_msg .= "Please write in the configuration file $main::EGROUPS_CONFIG the 'p_niceUserid'\n";

}

if(!$main::SECRET->{p_password}){
        $error_msg .= "Please write in the configuration file $main::EGROUPS_CONFIG the 'p_password'\n";
}

my $end_point;

if($main::SECRET->{end_point}){
        $end_point = $main::SECRET->{end_point};
}else{
        $end_point = $DEF{end_point};
}

my $xmlns;

if($main::SECRET->{xmlns}){
        $xmlns = $main::SECRET->{xmlns};
}else{
        $xmlns= $DEF{xmlns};
}


if(!$end_point){ $error_msg .= "NO end_point found\n"; }

if(!$xmlns){ $error_msg .= "NO xmlns found\n"; }

if($error_msg){
        print STDERR "$error_msg\n";
        exit 2;
}

my $soap_member;
my $soap = init_SOAP();

$soap->readable(1);

foreach my $user (@members){
# Add/remove it as a External if it is an email
# 	as an account otherwise
 if(&AFSLDAP::is_email($user)){
#	      $soap_member = SOAP::Data->name("members")->value({"Email"=>$user,"Type"=>"External"});

       	$soap_member = SOAP::Data->name( 'members' => \SOAP::Data->value(
#               "Name"=>$user
#               "Type"=>"StaticEgroup",
                SOAP::Data->new(name => 'Type', attr => {    
                  'xsi:type' => undef}, value => 'External'
                ),
                SOAP::Data->new(name => 'Email', attr => {
                  'xsi:type' => undef}, value => $user
                )
        ));


 } elsif ($user =~ /-/) {
	$soap_member = SOAP::Data->name( 'members' => \SOAP::Data->value(
#		"Name"=>$user,
#		"Type"=>"StaticEgroup",
		SOAP::Data->new(name => 'Type', attr => { 
  		  'xsi:type' => undef}, value => 'StaticEgroup'
		),
		SOAP::Data->new(name => 'Name', attr => {
                  'xsi:type' => undef}, value => $user
                )
	));

 } else {
	#$soap_member = SOAP::Data->name("members")->value({"Name"=>$user,"Type"=>"Account"});
	 $soap_member = SOAP::Data->name( 'members' => \SOAP::Data->value(
#               "Name"=>$user,
#               "Type"=>"StaticEgroup",
	        SOAP::Data->new(name => 'Type', attr => {    
                  'xsi:type' => undef}, value => 'Account'
                ),
                SOAP::Data->new(name => 'Name', attr => {
                  'xsi:type' => undef}, value => $user
                )
        ));
 }


#my $group_def=SOAP::Data->attr({"xmlns" => $xmlns })->value([@soap_members]);

my $soap_response=$soap->call(SOAP::Data->name($action)
      ->attr({xmlns=>$xmlns})
      => (
          #SOAP::Data->name("p_niceUserid" => $main::SECRET->{p_niceUserid}),
          #SOAP::Data->name("p_password" => $main::SECRET->{p_password}),
          SOAP::Data->name("egroupName" => $group),
	        SOAP::Data->name("overwriteMembers" => "false"),  # This is only for add, but del doesn't seem to care	
          $soap_member,
        )
);

if ($soap_response->result) {
 print "Server response ($group): ".Dumper($soap_response->result)."\n"; #should be "true" if success
}

if ($soap_response->faultstring) {
 print STDERR "SOAP Error ($group): ".Dumper($soap_response->faultstring)."\n";
 exit 1;
}

}

}

sub removeEgroupMembers($@){
my ($group,@members) = @_;

addremoveEgroupMembers("RemoveEgroupMembersRequest",$group,@members);

}

sub addEgroupMembers($@){
my ($group,@members) = @_;

addremoveEgroupMembers("AddEgroupMembersRequest",$group,@members);

}

sub print_usage() 
{
print "\nUsage: $0 <--get|--delete|--add|--new> egroup\n";
print "\t--get			: Uses LDAP to get the users in an egroup.\n";
print "\t--linear		: Outputs the users of get as a comma seperated string\n";
print "\t--add			: Adds the list of members to the egroup.\n";
print "\t--delete		: Removes the list of members to the egroup.\n";
print "\t--remove		: Removes the list of members to the egroup.\n";
print "\t--new			: Creates a new egroup with the list of members.\n\n";
print "\t--egroupremove   : Removes an egroup.\n\n";
print "Options:\n";
print "\t--members		: List of members\n";
print "\t--description		: Only for --new. It sets the description of the new group differently than the default one (".$DEF{Description}.").\n";
print "\t--privacy		: Only for --new. It sets the privacy of the new group differently than the default one (".$DEF{Privacy}.").\n";
print "\t--usage		        : Only for --new. It sets the usage code of the egroup differently than the default one (".$DEF{UsageCode}.").\n";
print "\t--selfsubscription	: Only for --new. It sets the self subscription of the new group differently than the default one (".$DEF{Selfsubscription}.").\n";
print "\t--owner		        : Only for --new. It sets the owner of the new group differently than the user who runs this (".$ENV{USER}.")\n";
print "\t--externals             : If omited, all externals will be ignored.\n";
print "\t--debug		        : Show debug information\n";
print "\t--inversiverecursive    : Use the inversive recursive method instead of the standard to get the users";
print "\n\n";
print "Examples:\n";
print "\t$0 --get VC-Administration\n";
print "\t$0 --add egroup-test --members ".$ENV{USER}."\@cern.ch ".$ENV{USER}."\n";
print "\nThe config file is searched first at ".$DEF{egroups_config_user}.", and if not found at ".$DEF{egroups_config}.". To change this, set the shell variable EGROUPS_CONFIG.\n";
print "\n";
}

sub getConfig (%) {
        my %args = @_;
        my %CONFIG = ();

        open(CONFIG, "<$args{'conf_file'}")
                 or print STDERR "No conf file $args{'conf_file'} $!. ";

        while(<CONFIG>) {
            chomp($_);
            if(/\s*(\S+)\s*=\s*([^#]*\w)\s*/){ 
                $CONFIG{$1} = $2;
            } 
        }
     
         close CONFIG or warn "Cannot close $args{'conf_file'} file.";
        # Why was this line commented out??
        ## Because it was breaking the command if the egroups is missing

        \%CONFIG;
}

sub get_password(){
  use Term::ReadKey;

print "password: ";

ReadMode('noecho');
ReadMode('raw');
my $pass = '';

while (1) {
  my $c;
  1 until defined($c = ReadKey(-1));
  last if $c eq "\n";
  print "*";
  $pass .= $c;
}

print "\n";

ReadMode('restore');
}

#-----------------------------------------------------------------------------
# Main
#-----------------------------------------------------------------------------

my ($help, $get, $externals, $delete, $remove, $egroupremove, $add, $new, @members, $Description, $Privacy,
        $Selfsubscription, $owner, $inversiverecursive, $AdministratorEgroup, $UsageCode, $linear);

our $debug;
# Where the options are defined
GetOptions('help|?' => \$help,
           'get=s' => \$get,
           'externals' => \$externals,
           'remove=s' => \$remove,
           'delete=s' => \$delete,
	   'egroupremove=s' => \$egroupremove,
           'add=s' => \$add,
           'new=s' => \$new,
           'members=s{1,}' => \@members,
           'description=s' => \$Description,
           'privacy=s' => \$Privacy,
           'usage=s' => \$/,
           'selfsubscription=s' => \$Selfsubscription,
           'owner=s' => \$owner,
           'debug' => \$debug,
           'inversiverecursive' => \$inversiverecursive,
           'administratoregroup=s' => \$AdministratorEgroup,
           'linear' => \$linear
);
#

my $EGROUPS_CONFIG;

if($ENV{EGROUPS_CONFIG}){
	$main::EGROUPS_CONFIG = $ENV{EGROUPS_CONFIG};
}elsif( -e $DEF{egroups_config_user}) { 
	$main::EGROUPS_CONFIG = $DEF{egroups_config_user};
} else {
	$main::EGROUPS_CONFIG = $DEF{egroups_config};
}

warn "Using $main::EGROUPS_CONFIG for configuration" if $debug;

my $SECRET;
$main::SECRET = &getConfig( conf_file => $main::EGROUPS_CONFIG );

if ($main::debug) {
    eval "use SOAP::Lite +trace => 'debug';";
} else {
    eval "use SOAP::Lite;";
}

if($help){
	print_usage();
	exit 0;
}elsif($get){
	my @new_user;
  if(!$inversiverecursive) {
    @new_user = afsgetUsers($get,$externals);
  } else {
	  @new_user=getUsers($get,$externals);
	}

  if ($linear){
		my $line = shift(@new_user);
		foreach my $user (@new_user){
			$line .= ", $user";
		}
		$line .="\n";
		print $line;
	}
	else{
		foreach my $user (@new_user){
			print "$user\n";
		}
	}
}elsif($remove){
	removeEgroupMembers($remove,@members);
}elsif($delete){
        removeEgroupMembers($delete,@members);
}elsif($add){
	addEgroupMembers($add,@members);
}elsif($new){
	if(!$Privacy){ $Privacy=$DEF{Privacy}; }
	if(!$Selfsubscription){ $Selfsubscription=$DEF{Selfsubscription}; }
	if(!$Description) { $Description = $DEF{Description}; }
	if(!$owner) { $owner=0; }
	if(!$UsageCode) { $UsageCode=$DEF{UsageCode}; }
	if(!$AdministratorEgroup && $main::SECRET->{AdministratorEgroup}){ $AdministratorEgroup=$SECRET->{AdministratorEgroup}; }
	if(!$AdministratorEgroup) { $AdministratorEgroup=0; }
	new_egroup($new,$Privacy,$Selfsubscription,$Description,$owner,$AdministratorEgroup,$UsageCode,@members);
} elsif($egroupremove){
  deleteEgroup($egroupremove);
}else{ 
	print_usage();
        exit 1;
}
