#-------------------------------------------------------------------------------
# AFSLDAP.pm
#
# Module for LDAP queries (and some helper routines). 
#
# @(#) $Header: /afs/cern.ch/project/afs/dev/aim/AFSLDAP.pm,v 1.11 2011/06/07 14:52:45 wiebalck Exp $
#
# Initial version by Arne Wiebalck IT-DSS/FDO, 2010.
#-------------------------------------------------------------------------------

package AFSLDAP;

use strict;
use warnings;
use Net::LDAP;

use Data::Dumper;

#------------------------------------------------------------------------------
# Defaults
#------------------------------------------------------------------------------

# Add check in case $ENV{HOME} is empty
#if(! $ENV{HOME}){
#    $ENV{HOME} = '.';
#}
#

my %DEF = (# 'ldap_base'=> 'OU=e-groups,OU=Workgroups,dc=cern,dc=ch',
    'ldap_base'=> 'dc=cern,dc=ch',
    'ldap_server' => 'xldap.cern.ch',
    'ldap_base_user' => 'OU=Users,OU=Organic Units,dc=cern,dc=ch',
    #'end_point' => 'https://cra-ws.cern.ch/cra-ws/',
    'end_point' => 'https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService',
    #'xmlns' => 'https://cra-ws.cern.ch/cra-ws/',
    'xmlns' => 'https://foundservices.cern.ch/ws/egroups/v1/schema/EgroupsServicesSchema',
    'Selfsubscription' => 'Closed',
    'UsageCode' => 'SecurityMailing',
    'Description' => 'No description',
    'Privacy' => 'Members',
    'Selfsubscription' => 'Closed',
#   'AdministratorEgroup' => 'VC-Administration', # This is only for VC systems
    'egroups_config' => '/etc/egroups.cfg',
    'egroups_config_user' => "$ENV{HOME}/.egroups/config",
);

use IO::Socket;

our $do_externals;
our $debug;

my $EGROUPS_CONFIG;

if($ENV{EGROUPS_CONFIG}){
    $EGROUPS_CONFIG = $ENV{EGROUPS_CONFIG};
}elsif( -e $DEF{egroups_config_user}) { 
    $EGROUPS_CONFIG = $DEF{egroups_config_user};
} else {
    $EGROUPS_CONFIG = $DEF{egroups_config};
}

warn "Using $EGROUPS_CONFIG for configuration" if $debug;


my $SECRET;
$AFSLDAP::SECRET = &getConfig( conf_file => $EGROUPS_CONFIG );

#-------------------------------------------------------------------------------
# Global variables
#-------------------------------------------------------------------------------

my $LDAPserver = '';

if($AFSLDAP::SECRET->{ldap_server}){
        $LDAPserver=$AFSLDAP::SECRET->{ldap_server};
}else{
        $LDAPserver=$DEF{ldap_server};
}

my $domain_root = '';

if($AFSLDAP::SECRET->{ldap_base}){
        $domain_root=$AFSLDAP::SECRET->{ldap_base};
}else{
        $domain_root=$DEF{ldap_base};
}

#-------------------------------------------------------------------------------
#
# getConfig
#
#-------------------------------------------------------------------------------

sub getConfig (%) {
        my %args = @_;
        my %CONFIG = ();

        open(CONFIG, "<$args{'conf_file'}")
                 or print STDERR "No conf file $args{'conf_file'} $!. ";

        while(<CONFIG>) {
            chomp($_);
            if(/\s*(\S+)\s*=\s*([^#]*\w)\s*/){ 
                $CONFIG{$1} = $2; 
            } 
        }
     
         close CONFIG or warn "Cannot close $args{'conf_file'} file.";
        # Why was this line commented out??
        ## Because it was breaking the command if the egroups is missing

        \%CONFIG;
}


#-------------------------------------------------------------------------------
#
# Interface functions 
#
#-------------------------------------------------------------------------------

sub is_LDAP_item() {

    # check if an LDAP item exists
    #
    # parameters: the item name, the object class
    #
    # returns   : 1 if the item exists in the given class
    #             0 otherwise

    my ($item, $class) = @_;

    # connect to the LDAP server

    warn "Using $LDAPserver." if $debug;

    my $ldap = Net::LDAP->new($LDAPserver);
    unless ($ldap) {
	my $ctr = 5;
	while (!$ldap && $ctr) {
	    warn "Could not create LDAP object, retrying in 5 secs ...\n";
	    $ctr--;
	    sleep 5;
	    $ldap = Net::LDAP->new($LDAPserver);
	}
	if (!$ldap && (0 == $ctr)) {
	    die "Could not create LDAP object after several attempts\n";
	}
    }

    # do the search
    my $mesg = $ldap->search(base   => $domain_root,
			     filter => "(&(cn=$item)(objectClass=$class))");
    if ($mesg->code) { 
	print STDERR "LDAP search error: ".$mesg->error."\n";
    }

    # disconnect
    $ldap->unbind or die "Cannot disconnect";	



    # there should be only one entry from our search
    return 1 if ($mesg->shift_entry);
    return 0;
}

sub is_LDAP_objectClass() {

    # check if an LDAP item is a from a given class
    #
    # parameters: the item name, the class to check
    #
    # returns   : 1 if the item exists
    #             0 otherwise

    my ($item, $class) = @_;
    
    # connect to the LDAP server
    my $ldap = Net::LDAP->new($LDAPserver);
    unless ($ldap) {
	my $ctr = 5;
	while (!$ldap && $ctr) {
	    warn "Could not create LDAP object, retrying in 5 secs ...\n";
	    $ctr--;
	    sleep 5;
	    $ldap = Net::LDAP->new($LDAPserver);
	}
	if (!$ldap && (0 == $ctr)) {
	    die "Could not create LDAP object after several attempts\n";
	}
    }

    # do the search
    my $mesg = $ldap->search(base   => $domain_root,
                             filter => "(cn=$item)",
                             attrs  => [ "objectClass" ]);
    if ($mesg->code) { 
        print STDERR "LDAP search error: ".$mesg->error."\n";
    }

    # disconnect
    $ldap->unbind;      

    # check the class
    my $entry = $mesg->shift_entry;
    if (defined $entry) {
    	foreach my $c ($entry->get_value("objectClass")) {
        	return 1 if ($c eq $class)
    	}
    }
    return 0;
}


sub get_egroup_members( $ ) {

    # get the members of an e-group group from LDAP
    #
    # parameters: the group name
    #
    # returns   : an array with the group members


    my $grp = shift;

    warn "Conecting to $LDAPserver" if $debug;

    my($addr)=inet_ntoa((gethostbyname($LDAPserver))[4]);
    my $ldap = Net::LDAP->new($addr); 
    unless ($ldap) {
	my %addrs = ();
        my $ctr = 15;
        while (!$ldap && $ctr) {
            warn "Could not connect to $addr, retrying in 5 secs ...\n";
	    $addrs{$addr}++;
            $ctr--;
            sleep 5;
            ($addr)=inet_ntoa((gethostbyname($LDAPserver))[4]);
            $ldap = Net::LDAP->new($addr);
        }
        if (!$ldap && (0 == $ctr)) {
            my $message="Could not create LDAP object after 15 attempts\n";
	    $message.="$_ $addrs{$_}\n" for (keys %addrs);
	    die $message;
        }
    }

 
#	"Cannot connect to $addr for $grp group. $@";
    #my $ldap = Net::LDAP->new($addr) or die "Cannot connect to $addr for $grp group. $@";

#    my $ldap = Net::LDAP->new($LDAPserver) or die "Cannot get $group members. $@";

    # do the search	
    my $mesg;
    my $step = 1499;
    my $low  = 0;
    my $high = $step;
    my @Amem = ();
    my $len  = 0;
    my $continue = 1;

    while ($continue) {

	$mesg = $ldap->search(base   => $domain_root,
			      filter => "(&(CN=$grp)(objectClass=group))",
			      attrs  => [ "member;range=$low-$high" ] );                
	if ($mesg->code) {
	    die "LDAP search error: ".$mesg->error."\n";
	}

    warn "Return code (".$mesg->code."): ".$mesg->error if $main::debug;
    print Dumper(\$mesg) if $main::debug;

	if (1 == $mesg->count) { 
	    my $entry = $mesg->shift_entry;
	    my @Atmp = $entry->get_value("member;range=$low-$high");
	    my $len = scalar @Atmp;
	    
	    if ($len) {
		push @Amem, $entry->get_value("member;range=$low-$high");
		$low = $high + 1;
		$high = $low + $step;
	    } else {
		push @Amem, $entry->get_value("member;range=$low-*");
		$continue = 0;
	    }
	} else {
	    $continue = 0;
	}
    }

    # disconnect
    $ldap->unbind;	
    return @Amem;
}

sub is_group($){
    my ($grp) = @_;

    my $ldap = ldap_connect();

    my $mesg = $ldap->search(base   => $domain_root,
                  filter => "(&(CN=$grp)(objectClass=group))",
                  attrs  => [ ] );                
    if ($mesg->code) {
        die "LDAP search error: ".$mesg->error."\n";
    }

    if ($mesg->count > 0){ 
        warn "Group $grp exists" if $main::debug;
        return 1;
    }
    else { return 0; }
}

sub ldap_connect(){
    warn "Conecting to $LDAPserver" if $debug;

    my($addr)=inet_ntoa((gethostbyname($LDAPserver))[4]);
    my $ldap = Net::LDAP->new($addr); 
    unless ($ldap) {
    my %addrs = ();
        my $ctr = 15;
        while (!$ldap && $ctr) {
            warn "Could not connect to $addr, retrying in 5 secs ...\n";
        $addrs{$addr}++;
            $ctr--;
            sleep 5;
            ($addr)=inet_ntoa((gethostbyname($LDAPserver))[4]);
            $ldap = Net::LDAP->new($addr);
        }
        if (!$ldap && (0 == $ctr)) {
            my $message="Could not create LDAP object after 15 attempts\n";
        $message.="$_ $addrs{$_}\n" for (keys %addrs);
        die $message;
        }
    }

 return $ldap;
}

sub get_egroup_members_flat(){  #$@%$) {

    # flatten out a given e-group recursively
    #
    # parameters: an e-group name
    #             the reference to the flat list
    #             the reference to a memory hash
    #
    # returns   : nothing

    my ($eg, $ar, $hr, $hur, $ver, $externals) = @_;

    our $do_externals=$externals;

    # protect from deep recursion
    if (exists $$hr{$eg}) {
        return;
    } else {
        $$hr{$eg} = 1;
    }

    # traverse the n-ary tree
    my @A = &get_egroup_members($eg);
    my @B = &AFSLDAP::extract_CNs_from_DNs(\@A,$ar, $hur);
    if (0 == @B) {
#        # omit non user leafs, such as empty groups
#        if (&is_LDAP_objectClass($eg, "user")) {
#            push @$ar, $eg;
#        } else {
#            if ($ver){
#                print STDERR "Found non-user $eg as a leaf\n";
#            }
#        }
    } else {
        foreach my $pg (@B) {
            &get_egroup_members_flat($pg, $ar, $hr, $hur, $ver);
        }       
    } 
    return;
}

sub extract_CNs_from_DNs(){    #@@) {

    # extract the CNs from an array of DNs
    #
    # parameters: an array with DNs
    #
    # returns   : an array with the CNs
    
    my ($ADNs,$users, $memory) = @_;
    my @ACNs = ();

    foreach my $dn (@$ADNs) {
	if ( $dn =~ /^CN=(.*),OU=(e-groups|Unix).*$/) {
	    push @ACNs, $1;
	}elsif ($dn =~ /^CN=(.*),OU=Users.*$/){
	    unless($$memory{$1}){
	        push @$users, $1;
		$$memory{$1}=1;
	    } 
	}elsif ($dn =~ /^CN=(.*),OU=[^,]*,OU=Externals.*$/){
     
	 if($do_externals){
            unless($$memory{$1}){
                push @$users, $1;
                $$memory{$1}=1;
	    }
	}
	 else { next; }

        } else {
	    print STDERR "Could not identify CN in in DN: $dn\n";
	    next;
	}
    }
    return @ACNs;
}

sub get_mail_address() {

    # get the mail address for an account
    #
    # parameters: an account name
    #
    # returns   : a mail address or an empty string if 
    #             the object was not found in LDAP

    my $account = shift;

    if ($account !~ /^[a-z][a-z0-9_-]+$/) {
	print STDERR "Illegal account name \'$account\'\n";
	return "";	
    }

    # connect to the LDAP server
    my $ldap = Net::LDAP->new($LDAPserver);
    unless ($ldap) {
	my $ctr = 5;
	while (!$ldap && $ctr) {
	    warn "Could not create LDAP object, retrying in 5 secs ...\n";
	    $ctr--;
	    sleep 5;
	    $ldap = Net::LDAP->new($LDAPserver);
	}
	if (!$ldap && (0 == $ctr)) {
	    die "Could not create LDAP object after several attempts\n";
	}
    }

    # do the search
    my $mesg = $ldap->search(base   => $domain_root,
                             filter => "(&(objectCategory=Person)(cn=$account))",
                             attrs  => [ "mail" ]);
    if ($mesg->code) { 
        print STDERR "LDAP search error: ".$mesg->error."\n";
    }

    # disconnect
    $ldap->unbind; 

    # get the mail address
    if (1 == $mesg->count) { 	
	my $entry = $mesg->shift_entry;
	return ($entry->get_value("mail"))[0];
    } else {
	print STDERR "Unable to find mail address for $account\n";
	return "";
    }
}

sub get_name() {

    # get the user's name for an account 
    #
    # parameters: an account name
    #
    # returns   : a string containing given name and surname

    my $account = shift;

    if ($account !~ /^[a-z][a-z0-9_-]+$/) {
	print STDERR "Illegal account name \'$account\'\n";
	return "";	
    }

    # connect to the LDAP server
    my $ldap = Net::LDAP->new($LDAPserver);
    unless ($ldap) {
	my $ctr = 5;
	while (!$ldap && $ctr) {
	    warn "Could not create LDAP object, retrying in 5 secs ...\n";
	    $ctr--;
	    sleep 5;
	    $ldap = Net::LDAP->new($LDAPserver);
	}
	if (!$ldap && (0 == $ctr)) {
	    die "Could not create LDAP object after several attempts\n";
	}
    }

    # do the search
    my $mesg = $ldap->search(base   => $domain_root,
                             filter => "(&(objectCategory=Person)(cn=$account))",
                             attrs  => [ "givenName", "sn" ]);
    if ($mesg->code) { 
        print STDERR "LDAP search error: ".$mesg->error."\n";
    }

    # disconnect
    $ldap->unbind; 

    # get the name
    if (1 == $mesg->count) { 	
	my $entry = $mesg->shift_entry;
	return (join(" ", $entry->get_value("givenName"),  $entry->get_value("sn")));
    } else {
	print STDERR "Unable to find name for $account\n";
	return "";
    }
}

sub is_email($){
my ($email) = @_;

 if($email =~ m/^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/){
    return 1;
 }else{
    return 0;
 }

}

1; 

__END__
