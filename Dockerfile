#
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
#
MAINTAINER Alvaro Gonzalez <agonzale@cern.ch>
#
RUN yum install -y perl perl-LDAP perl-TermReadKey perl-SOAP-Lite perl-LWP-Protocol-https && \
    ln -s /usr/bin/egroups.pl /usr/bin/egroups
#
COPY egroups*.pl /usr/bin/
COPY AFSLDAP.pm /usr/share/perl5/
#
ENTRYPOINT bash
#
