#!/usr/bin/env perl
use SOAP::Lite;
#use SOAP::Lite+trace;
#Uncomment next line for debugging
#use SOAP::Lite+debug;

if(!$ARGV[0]){
	print "Use: $0 <EGROUP>";
	exit 1;
}

#replace by your credentials:
my $password = '95Holts+berg32';
my $username = 'agonzale';
sub SOAP::Transport::HTTP::Client::get_basic_credentials {
	return $username =>$password;
};

#Get the services and authenticate
$soap=SOAP::Lite
	->uri("https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/")
        ->proxy("https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/", timeout=>10*60);

$soap->autotype(0)->readable(1);

$soap->default_ns('urn:https://foundservices.cern.ch/ws/egroups/v1/EgroupsWebService/');

 # Find an egroup by name
 $soap_response=$soap->call(SOAP::Data->name("FindEgroupByNameRequest")
                                     ->attr({xmlns=>"https://foundservices.cern.ch/ws/egroups/v1/schema/EgroupsServicesSchema"})
                      => ( SOAP::Data->name("name" => $ARGV[0])));


if ($soap_response->faultstring or not $soap_response->result) {
die "SOAP Error retrieving e-group: ".$soap_response->faultstring."\n";
}

#print "Current E-group owner: ".$soap_response->valueof('//FindEgroupByNameResponse/result/Owner/Name')."\n";
#print "Current E-group name: ".$soap_response->valueof('//FindEgroupByNameResponse/result/Name')."\n";
#print "Current Usage: ".$soap_response->valueof('//FindEgroupByNameResponse/result/Usage')."\n";
#print "Current Description: ".$soap_response->valueof('//FindEgroupByNameResponse/result/Description')."\n";
##
#print "Current Owner: ".$soap_response->valueof('//FindEgroupByNameResponse/result/Owner/PersonId')."\n";
#print "Current Privacy: ".$soap_response->valueof('//FindEgroupByNameResponse/result/Privacy')."\n";
#print "Current Selfsubscription: ".$soap_response->valueof('//FindEgroupByNameResponse/result/Selfsubscription')."\n";
#print "Current Administrator: ".$soap_response->valueof('//FindEgroupByNameResponse/result/AdministratorEgroup')."\n";

@oldmembers = $soap_response->valueof('//FindEgroupByNameResponse/result/Members');

@members = ();

# SynchronizeEgroup
#
my $file_no = scalar (@oldmembers);
for (my $i=0; $i < $file_no; $i++)
{
	$type = $oldmembers[$i]{Type};
	$id = $oldmembers[$i]{ID};

	if($type eq "External") {
		push @members,  SOAP::Data->value({"ID"=>0, "Type"=>$type, "Email"=> $oldmembers[$i]{Email}});
	} else {
		push @members,  SOAP::Data->value({"ID"=>$id, "Type"=>$type});
	}
}

my $group_def;

if(! scalar @members){

$group_def = SOAP::Data->name("egroup" =>
        \SOAP::Data->value(
                SOAP::Data->name("Name" => $soap_response->valueof('//FindEgroupByNameResponse/result/Name')),
                SOAP::Data->name("Type" => 'StaticEgroup'), # only this type is supported
                SOAP::Data->name("Usage" => $soap_response->valueof('//FindEgroupByNameResponse/result/Usage')),
                SOAP::Data->name("Description" => $soap_response->valueof('//FindEgroupByNameResponse/result/Description')),
                SOAP::Data->name("Owner" => \SOAP::Data->value(
                        SOAP::Data->name("PersonId" => $soap_response->valueof('//FindEgroupByNameResponse/result/Owner/PersonId')),
                )),
                SOAP::Data->name("Privacy" => $soap_response->valueof('//FindEgroupByNameResponse/result/Privacy')),
                SOAP::Data->name("Selfsubscription" => $soap_response->valueof('//FindEgroupByNameResponse/result/Selfsubscription'))
                ));

} else {
$group_def = SOAP::Data->name("egroup" =>
	\SOAP::Data->value(
		SOAP::Data->name("Name" => $soap_response->valueof('//FindEgroupByNameResponse/result/Name')),
		SOAP::Data->name("Type" => 'StaticEgroup'), # only this type is supported
		SOAP::Data->name("Usage" => $soap_response->valueof('//FindEgroupByNameResponse/result/Usage')),
		SOAP::Data->name("Description" => $soap_response->valueof('//FindEgroupByNameResponse/result/Description')),
		SOAP::Data->name("Owner" => \SOAP::Data->value(
			SOAP::Data->name("PersonId" => $soap_response->valueof('//FindEgroupByNameResponse/result/Owner/PersonId')),
		)),
		SOAP::Data->name("Privacy" => $soap_response->valueof('//FindEgroupByNameResponse/result/Privacy')),
		SOAP::Data->name("Selfsubscription" => $soap_response->valueof('//FindEgroupByNameResponse/result/Selfsubscription')),
		SOAP::Data->name("Members" => SOAP::Data->attr({xmlns=>"https://foundservices.cern.ch/ws/egroups/v1/schema/EgroupsServicesSchema"})->value(@members)
		)
	 	));
}

my $soap_response=$soap->call(SOAP::Data->name("SynchronizeEgroupRequest")
	->attr({xmlns=>'https://foundservices.cern.ch/ws/egroups/v1/schema/EgroupsServicesSchema'})
	=> (
		$group_def
	));

if ($soap_response->faultstring or not $soap_response->result) {
	die "SOAP Error ".$soap_response->faultstring."\n";
}
